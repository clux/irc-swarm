# irc-swarm
[![dependency status](https://david-dm.org/clux/irc-swarm.svg)](https://david-dm.org/clux/irc-swarm)

Version manages the following bots:

 * [cleverbot-irc](https://github.com/clux/cleverbot-irc)
 * [wolfram-irc](https://github.com/clux/wolfram-irc)

as well as their configuration files.

## Usage
Deploy an openstack VM and deploy to it:

```sh
nova boot --flavor Micro-Small --image "Debian Jessie 8.0.0-3" --key-name $OS_USERNAME irc-swarm
ip=$(nova list | grep "irc-swarm" | grep -oE "public=[0-9.]*" | sed "s/public=//")
ssh debian@$ip

wget -qO- https://raw.githubusercontent.com/creationix/nvm/v0.25.1/install.sh | bash
. .bashrc
nvm install 0.10 # can't use iojs@2.0.1 at least - nan broken

sudo apt-get update
sudo apt-get install git build-essential libxml2-dev -y
npm install pm2 -g
```

Move loading of nvm to the top of `.bashrc`, and add the line `nvm use 0.10` right after. This is to prevent non-interactive shells (ssh conn from pm2) to return early. TODO: maybe there is a better way to do this...

First update the ip in the `ecosystem.json` to `$ip`, then commit and push.

Then you should be able to deploy to it remotely:

```sh
npm run deploy:init
npm run deploy:deps
npm run deploy:code
```

Finally, ensure it starts up when the VM image reboots:

```sh
pm2 startup # follow
pm2 save
```
